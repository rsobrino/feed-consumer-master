package com.sparta.server.service;

import com.sparta.server.model.Record;
import com.sparta.server.model.Sensor;
import com.sparta.server.model.SensorCollection;
import com.sparta.server.repository.MainRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
class MainServiceTest {

    @Mock
    private MainRepository mainRepository;

    @InjectMocks
    private MainService mainService;


    @Test
    void load() throws IOException {

        //Given
        List<Record> dato = Arrays.asList(generateRecord(), generateRecord(), generateRecord());

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeLong(3);
        for (Record record : dato) {
            dataOutputStream.write(transformRecord(record));
        }
        dataOutputStream.flush();
        byte[] bytes = byteArrayOutputStream.toByteArray();

        when(mainRepository.save("repsol", dato)).thenReturn(dato.size());

        //Then
        int resultado = this.mainService.load("repsol", bytes);


        //When
        assertEquals(dato.size(), resultado);
    }

    @Test
    void readRecord() throws IOException {
        //Given
        Record dato = generateRecord();

        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(transformRecord(dato)));

        //Then
        Record resultado = this.mainService.readRecord(dataInputStream);


        //When
        assertEquals(dato, resultado);

    }

    private Record generateRecord() throws IOException {
        Record dato = new Record();
        dato.setRecordIndex(12);
        SensorCollection sensorCollection = new SensorCollection();
        sensorCollection.setSensors(Arrays.asList(new Sensor("erttrt", 4), new Sensor("ertreer", 1),
                new Sensor("erttreert", 7), new Sensor("rrrretttt", 6)));
        sensorCollection.setNumberOfSensors(4);
        dato.setSensorsData(sensorCollection);
        dato.setCity("Madrid");
        dato.setCrc32SensorsData(231233);
        dato.setTimestamp(8882727);
        dato.setNumberBytesSensorData(this.transformSensorCollection(dato.getSensorsData()).length);
        return dato;
    }

    private byte[] transformRecord(Record record) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeLong(record.getRecordIndex());
        dataOutputStream.writeLong(record.getTimestamp());

        writeString(dataOutputStream, record.getCity());
        byte[] batchBytes = this.transformSensorCollection(record.getSensorsData());
        dataOutputStream.writeInt(batchBytes.length);
        dataOutputStream.write(batchBytes);
        dataOutputStream.writeLong(record.getCrc32SensorsData());
        dataOutputStream.flush();
        return byteArrayOutputStream.toByteArray();
    }

    @Test
    void readStringProp() throws IOException {
        //Given
        String dato = "PRuebassssss----333";
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(transformString(dato)));

        //Then
        String resultado = this.mainService.readStringProp(dataInputStream);


        //When
        assertEquals(dato, resultado);

    }

    private byte[] transformString(String dato) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

        writeString(dataOutputStream, dato);
        dataOutputStream.flush();
        return byteArrayOutputStream.toByteArray();
    }


    private byte[] transformSensorCollection(SensorCollection sensorCollection) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        dataOutputStream.writeInt(sensorCollection.getNumberOfSensors());
        for (Sensor sensor : sensorCollection.getSensors()) {
            writeString(dataOutputStream, sensor.getId());
            dataOutputStream.writeInt(sensor.getMeasure());
        }
        dataOutputStream.flush();
        return byteArrayOutputStream.toByteArray();
    }

    private static void writeString(DataOutputStream dos, String dato) throws IOException {
        final byte[] cityBytes = dato.getBytes();
        dos.writeInt(cityBytes.length);
        dos.write(cityBytes);
    }

    @Test
    void readSensorCollection() throws IOException {
        //Given
        SensorCollection sensorCollection = new SensorCollection();
        sensorCollection.setSensors(Arrays.asList(new Sensor("erttrt", 4), new Sensor("ertreer", 1),
                new Sensor("erttreert", 7), new Sensor("rrrretttt", 6)));
        sensorCollection.setNumberOfSensors(4);

        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(transformSensorCollection(sensorCollection)));

        //Then
        SensorCollection resultado = this.mainService.readSensorCollection(dataInputStream);


        //When
        assertEquals(sensorCollection, resultado);

    }

    @Test
    void total() {
        //Given
        when(mainRepository.total("repsol")).thenReturn(4);

        //Then
        int resultado = this.mainService.total("repsol");


        //When
        assertEquals(4, resultado);
    }
}