package com.sparta.server.repository;

import com.sparta.server.model.Record;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MainRepository {

    Map<String, List<Record>> records;


    @PostConstruct
    private void init() {
        this.records = new HashMap<>();
    }


    public int save(String provider, List<Record> records) {
        if (this.records.containsKey(provider) && this.records.get(provider) != null) {
            this.records.get(provider).addAll(records);
        } else {
            this.records.put(provider, records);
        }
        return records.size();
    }


    public int total(String provider) {
        return this.records.containsKey(provider) ? this.records.get(provider).size() : 0;
    }


}
