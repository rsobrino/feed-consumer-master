package com.sparta.server.controller;

import com.sparta.server.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class MainController {

    @Autowired
    private MainService mainService;

    @PostMapping("/load/{provider}")
    public int load(@PathVariable("provider") String provider, @RequestBody byte[] content) throws IOException {

        return mainService.load(provider, content);
    }

    @GetMapping("/data/{provider}/total")
    public int total(@PathVariable("provider") String provider) {

        return mainService.total(provider);
    }

}
