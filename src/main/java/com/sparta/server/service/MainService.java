package com.sparta.server.service;

import com.sparta.server.model.Record;
import com.sparta.server.model.Sensor;
import com.sparta.server.model.SensorCollection;
import com.sparta.server.repository.MainRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MainService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MainRepository mainRepository;

    public int load(String provider, byte[] content) throws IOException {

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content);
        DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
        long total = dataInputStream.readLong();

        logger.info("Reading a total of {} records from {}",total, provider);

        List<Record> records = new ArrayList<>();
        for (int i = 0; i < total; i++) {
            records.add(readRecord(dataInputStream));
        }

        return mainRepository.save(provider, records);
    }

    protected Record readRecord(DataInputStream dataInputStream) throws IOException {
        Record record = new Record();

        record.setRecordIndex(dataInputStream.readLong());
        record.setTimestamp(dataInputStream.readLong());
        record.setCity(readStringProp(dataInputStream));
        record.setNumberBytesSensorData(dataInputStream.readInt());
        record.setSensorsData(readSensorCollection(dataInputStream));
        record.setCrc32SensorsData(dataInputStream.readLong());

        logger.info("Reading {}", record);
        return record;
    }

    protected String readStringProp(DataInputStream dataInputStream) throws IOException {

        int total = dataInputStream.readInt();

        StringBuilder dato = new StringBuilder();
        for (int i = 0; i < total; i++) {
            dato.append((char) (dataInputStream.readByte()));
        }
        return dato.toString();
    }

    protected SensorCollection readSensorCollection(DataInputStream dataInputStream) throws IOException {
        SensorCollection sensorCollection = new SensorCollection();
        int total = dataInputStream.readInt();

        List<Sensor> sensors = new ArrayList<>();
        for (int i = 0; i < total; i++) {
            Sensor sensor = new Sensor();
            sensor.setId(readStringProp(dataInputStream));
            sensor.setMeasure(dataInputStream.readInt());
            sensors.add(sensor);
        }
        sensorCollection.setNumberOfSensors(total);
        sensorCollection.setSensors(sensors);
        return sensorCollection;
    }

    public int total(String provider) {

        return mainRepository.total(provider);
    }
}
