package com.sparta.server.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SensorCollection implements Serializable {

    private int numberOfSensors;
    private List<Sensor> sensors;

    public SensorCollection(int numberOfSensors, List<Sensor> sensors) {
        this.numberOfSensors = numberOfSensors;
        this.sensors = sensors;
    }

    public SensorCollection() {
        sensors = new ArrayList<>();
    }

    public int getNumberOfSensors() {
        return numberOfSensors;
    }

    public void setNumberOfSensors(int numberOfSensors) {
        this.numberOfSensors = numberOfSensors;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorCollection that = (SensorCollection) o;
        return numberOfSensors == that.numberOfSensors &&
                Objects.equals(sensors, that.sensors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfSensors, sensors);
    }

    @Override
    public String toString() {
        return "SensorCollection{" +
                "numberOfSensors=" + numberOfSensors +
                ", sensors=" + sensors +
                '}';
    }
}
