package com.sparta.server.model;

import java.io.Serializable;
import java.util.Objects;

public class Record implements Serializable {

    private long recordIndex;
    private long timestamp;
    private String city;
    private int numberBytesSensorData;
    private SensorCollection sensorsData;
    private long crc32SensorsData;


    public Record(long recordIndex, long timestamp, String city, int numberBytesSensorData, SensorCollection sensorsData, long crc32SensorsData) {
        this.recordIndex = recordIndex;
        this.timestamp = timestamp;
        this.city = city;
        this.numberBytesSensorData = numberBytesSensorData;
        this.sensorsData = sensorsData;
        this.crc32SensorsData = crc32SensorsData;
    }

    public Record() {

    }

    public long getRecordIndex() {
        return recordIndex;
    }

    public void setRecordIndex(long recordIndex) {
        this.recordIndex = recordIndex;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getNumberBytesSensorData() {
        return numberBytesSensorData;
    }

    public void setNumberBytesSensorData(int numberBytesSensorData) {
        this.numberBytesSensorData = numberBytesSensorData;
    }

    public SensorCollection getSensorsData() {
        return sensorsData;
    }

    public void setSensorsData(SensorCollection sensorsData) {
        this.sensorsData = sensorsData;
    }

    public long getCrc32SensorsData() {
        return crc32SensorsData;
    }

    public void setCrc32SensorsData(long crc32SensorsData) {
        this.crc32SensorsData = crc32SensorsData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return recordIndex == record.recordIndex &&
                timestamp == record.timestamp &&
                numberBytesSensorData == record.numberBytesSensorData &&
                crc32SensorsData == record.crc32SensorsData &&
                Objects.equals(city, record.city) &&
                Objects.equals(sensorsData, record.sensorsData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recordIndex, timestamp, city, numberBytesSensorData, sensorsData, crc32SensorsData);
    }

    @Override
    public java.lang.String toString() {
        return "Record{" +
                "recordIndex=" + recordIndex +
                ", timestamp=" + timestamp +
                ", city='" + city + '\'' +
                ", numberBytesSensorData=" + numberBytesSensorData +
                ", sensorsData=" + sensorsData +
                ", crc32SensorsData=" + crc32SensorsData +
                '}';
    }
}
