package com.sparta.server.model;

import java.io.Serializable;
import java.util.Objects;

public class Sensor implements Serializable {


    private String id;
    private int measure;

    public Sensor() {
    }

    public Sensor(String id, int measure) {
        this.id = id;
        this.measure = measure;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMeasure() {
        return measure;
    }

    public void setMeasure(int measure) {
        this.measure = measure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sensor sensor = (Sensor) o;
        return measure == sensor.measure &&
                Objects.equals(id, sensor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, measure);
    }

    @Override
    public java.lang.String toString() {
        return "Sensor{" +
                "id='" + id + '\'' +
                ", measure=" + measure +
                '}';
    }
}
